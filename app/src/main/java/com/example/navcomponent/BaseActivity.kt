package com.example.navcomponent

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {

    @SuppressLint("RestrictedApi")
    fun showActionBar() {
//        supportActionBar?.setShowHideAnimationEnabled(false)
        supportActionBar?.show()
    }

    @SuppressLint("RestrictedApi")
    fun hideActionBar() {
//        supportActionBar?.setShowHideAnimationEnabled(false)
        supportActionBar?.hide()
    }
}
package com.example.navcomponent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.viewbinding.ViewBinding

typealias Inflate<T> = (LayoutInflater) -> T

fun NavController.safeNavigate(direction: NavDirections, navOptions: NavOptions?) {
    currentDestination?.getAction(direction.actionId)?.run { navigate(direction, navOptions) }
}

abstract class BaseFragment<out Binding : ViewBinding>(
    private val inflate: Inflate<Binding>
) : Fragment() {
    protected val layout: Binding by lazy {
        inflate.invoke(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return layout.root
    }

    protected fun getNavOptionsHorizontal(): NavOptions.Builder {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_right)
            .setExitAnim(R.anim.slide_out_left)
            .setPopEnterAnim(R.anim.slide_in_left)
            .setPopExitAnim(R.anim.slide_out_right)
    }

    protected fun getNavOptionsVertical(): NavOptions.Builder {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_bottom)
            .setExitAnim(R.anim.slide_out_top)
            .setPopEnterAnim(R.anim.slide_in_top)
            .setPopExitAnim(R.anim.slide_out_bottom)
    }

    protected fun getNavOptionsVertical2(): NavOptions.Builder {
        return NavOptions.Builder()
            .setEnterAnim(R.anim.slide_in_top)
            .setExitAnim(R.anim.slide_out_bottom)
            .setPopEnterAnim(R.anim.slide_in_bottom)
            .setPopExitAnim(R.anim.slide_out_top)
    }
}
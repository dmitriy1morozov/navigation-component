package com.example.navcomponent

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SomeArgument(val arg1: Int, val arg2: String) : Parcelable {
}
package com.example.navcomponent.main

import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.R
import com.example.navcomponent.SomeArgument
import com.example.navcomponent.databinding.FragmentOneBinding
import com.example.navcomponent.safeNavigate

class FragmentOne : BaseFragment<FragmentOneBinding>(FragmentOneBinding::inflate) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnFragmentTwo.setOnClickListener {
            val oneTwoDirections = FragmentOneDirections.oneTwo(SomeArgument(123, "test_string"), 444)
            requireActivity().findNavController(R.id.navHostFragmentSmall).safeNavigate(oneTwoDirections, getNavOptionsHorizontal().build())
        }

        layout.btnFragmentThree.setOnClickListener {
            val oneThreeDirections = FragmentOneDirections.oneThree()
            findNavController().safeNavigate(oneThreeDirections, getNavOptionsVertical().build())
        }
    }
}
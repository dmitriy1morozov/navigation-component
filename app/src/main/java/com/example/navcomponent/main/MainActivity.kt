package com.example.navcomponent.main

import android.os.Bundle
import com.example.navcomponent.BaseActivity
import com.example.navcomponent.R

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
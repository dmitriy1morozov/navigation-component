package com.example.navcomponent.main

import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.databinding.FragmentTwoBinding

class FragmentTwo : BaseFragment<FragmentTwoBinding>(FragmentTwoBinding::inflate) {

    private val args: FragmentTwoArgs by navArgs()

    override fun onResume() {
        super.onResume()

        val intArg = args.someArgs?.arg1 ?: 0
        val stringArg = args.someArgs?.arg2 ?: "EmptyString"
        Toast.makeText(requireContext(), "id = ${args.someId ?: "0"}", Toast.LENGTH_SHORT).show()
        Toast.makeText(requireContext(), "IntArg = $intArg", Toast.LENGTH_SHORT).show()
        Toast.makeText(requireContext(), "StringArg = $stringArg", Toast.LENGTH_SHORT).show()
    }
}
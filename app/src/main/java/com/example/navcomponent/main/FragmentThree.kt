package com.example.navcomponent.main

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseActivity
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.databinding.FragmentThreeBinding
import com.example.navcomponent.safeNavigate

class FragmentThree : BaseFragment<FragmentThreeBinding>(FragmentThreeBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnFragmentFour.setOnClickListener {
            val threeFourDirections = FragmentThreeDirections.threeFour()
            findNavController().safeNavigate(threeFourDirections, getNavOptionsVertical2().build())
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as? BaseActivity)?.hideActionBar()
    }

    override fun onPause() {
        super.onPause()
        (requireActivity() as? BaseActivity)?.showActionBar()
    }
}
package com.example.navcomponent.main

import android.os.Bundle
import android.view.View
import androidx.navigation.NavDeepLinkBuilder
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.R
import com.example.navcomponent.databinding.FragmentFourBinding
import com.example.navcomponent.safeNavigate
import com.example.navcomponent.second.SecondActivity

class FragmentFour : BaseFragment<FragmentFourBinding>(FragmentFourBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnBack.setOnClickListener {
            findNavController().navigate(
                R.id.four_back,
                null,
                getNavOptionsHorizontal()
                    .setPopUpTo(R.id.fragmentOne, true)
                    .build()
            )
        }

        layout.btnSecondActivity.setOnClickListener {
            val directions = FragmentFourDirections.fourSecondActivity()
            findNavController().safeNavigate(directions, getNavOptionsHorizontal().build())
        }

        layout.btnDeepLink.setOnClickListener {
//            findNavController().navigate("navcomponent://com.example.navcomponent/fragment_second_four".toUri())

            NavDeepLinkBuilder(requireContext())
                .setComponentName(SecondActivity::class.java)
                .setGraph(R.navigation.nav_graph_second)
                .setDestination(R.id.fragmentSecondFour)
                .createTaskStackBuilder()
                .startActivities()
        }
    }
}
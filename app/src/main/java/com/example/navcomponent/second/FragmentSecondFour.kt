package com.example.navcomponent.second

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.R
import com.example.navcomponent.databinding.FragmentSecondFourBinding

class FragmentSecondFour : BaseFragment<FragmentSecondFourBinding>(FragmentSecondFourBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnBack.setOnClickListener {
            findNavController().navigate(
                R.id.four_back,
                null,
                getNavOptionsHorizontal()
                    .setPopUpTo(R.id.fragmentSecondOne, true)
                    .build()
            )
        }
    }
}
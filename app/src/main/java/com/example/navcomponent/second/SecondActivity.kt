package com.example.navcomponent.second

import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import com.example.navcomponent.BaseActivity
import com.example.navcomponent.R

class SecondActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        findNavController(R.id.navHostFragment).handleDeepLink(intent)
    }
}
package com.example.navcomponent.second

import com.example.navcomponent.BaseFragment
import com.example.navcomponent.databinding.FragmentSecondTwoBinding

class FragmentSecondTwo : BaseFragment<FragmentSecondTwoBinding>(FragmentSecondTwoBinding::inflate) {

}
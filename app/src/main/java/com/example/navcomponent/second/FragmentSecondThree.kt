package com.example.navcomponent.second

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.databinding.FragmentSecondThreeBinding
import com.example.navcomponent.main.FragmentThreeDirections
import com.example.navcomponent.safeNavigate

class FragmentSecondThree : BaseFragment<FragmentSecondThreeBinding>(FragmentSecondThreeBinding::inflate) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnFragmentFour.setOnClickListener {
            findNavController().safeNavigate(FragmentThreeDirections.threeFour(), getNavOptionsHorizontal().build())
        }
    }
}
package com.example.navcomponent.second

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.example.navcomponent.BaseFragment
import com.example.navcomponent.databinding.FragmentSecondOneBinding
import com.example.navcomponent.safeNavigate

class FragmentSecondOne : BaseFragment<FragmentSecondOneBinding>(FragmentSecondOneBinding::inflate) {


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.btnFragmentTwo.setOnClickListener {
            findNavController().navigate(FragmentSecondOneDirections.oneTwo())
        }

        layout.btnFragmentThree.setOnClickListener {
            findNavController().safeNavigate(FragmentSecondOneDirections.oneThree(), getNavOptionsHorizontal().build())
        }
    }
}